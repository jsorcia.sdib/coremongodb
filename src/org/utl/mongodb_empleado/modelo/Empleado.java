/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.utl.mongodb_empleado.modelo;

/**
 *
 * @author Sorxia
 * 
 */
public class Empleado {
   private String idEmpledo;
   private String nombre;
   private String apellidoPa;
   private String apellidoMa;
   private String fechaNacimiento;
   private String genero;
   private String email;
   private String telefono;
   private String contrasena;
   private String nombreUsuario;
   private String estatus;
   private int rol;
   private String foto;
   private String fechaRegistro;
   private String claveAcceso;

    public String getIdEmpledo() {
        return idEmpledo;
    }

    public void setIdEmpledo(String idEmpledo) {
        this.idEmpledo = idEmpledo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellidoPa() {
        return apellidoPa;
    }

    public void setApellidoPa(String apellidoPa) {
        this.apellidoPa = apellidoPa;
    }

    public String getApellidoMa() {
        return apellidoMa;
    }

    public void setApellidoMa(String apellidoMa) {
        this.apellidoMa = apellidoMa;
    }

    public String getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(String fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getContrasena() {
        return contrasena;
    }

    public void setContrasena(String contrasena) {
        this.contrasena = contrasena;
    }

    public String getNombreUsuario() {
        return nombreUsuario;
    }

    public void setNombreUsuario(String nombreUsuario) {
        this.nombreUsuario = nombreUsuario;
    }

    public String getEstatus() {
        return estatus;
    }

    public void setEstatus(String estatus) {
        this.estatus = estatus;
    }

    public int getRol() {
        return rol;
    }

    public void setRol(int rol) {
        this.rol = rol;
    }

    public String getFoto() {
        return foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }

    public String getFechaRegistro() {
        return fechaRegistro;
    }

    public void setFechaRegistro(String fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }

    public String getClaveAcceso() {
        return claveAcceso;
    }

    public void setClaveAcceso(String claveAcceso) {
        this.claveAcceso = claveAcceso;
    }

    public Empleado() {
        this.idEmpledo = "";
        this.nombre = "";
        this.apellidoPa = "";
        this.apellidoMa = "";
        this.fechaNacimiento = "";
        this.genero = "";
        this.email = "";
        this.telefono = "";
        this.contrasena = "";
        this.nombreUsuario = "";
        this.estatus = "";
        this.rol = 0;
        this.foto = "";
        this.fechaRegistro = "";
        this.claveAcceso = "";
    }
    
    
}

package org.utl.mongodb_empleado.controlador;

import com.google.gson.Gson;
import com.mongodb.BasicDBObject;
import com.mongodb.Block;
import com.mongodb.MongoClient;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.model.UpdateOptions;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Random;
import org.bson.Document;
import org.bson.types.ObjectId;
import org.utl.mongodb_empleado.bd.Conexion;
import org.utl.mongodb_empleado.modelo.Empleado;

/**
 *
 * @author Sorxia
 */
public class EmpleadoControlador {

    Conexion conexion = new Conexion();

    public void connectDatabase() {
        conexion.setMongoClient(new MongoClient("localhost", 27017));
        conexion.setMongodb(conexion.getMongoClient().getDatabase("AccesOffice"));
    }

    public ArrayList montrarEmpleados() {
        BasicDBObject query = new BasicDBObject();
        query.append("estatus", "Activo");
        ArrayList documentos = new ArrayList();

        FindIterable<Document> iterable = conexion.getMongodb().getCollection("empleado").find(query);
        iterable.forEach(new Block<Document>() {
            @Override
            public void apply(final Document document) {
                documentos.add(document);
            }
        });
        return documentos;
    }

    public void insertarEmpleados(Empleado em) throws NoSuchAlgorithmException {
        Document empleado = new Document();
        empleado.append("nombre", em.getNombre())
                .append("apellidoPa", em.getApellidoPa())
                .append("apellidoMa", em.getApellidoMa())
                .append("fechaNacimiento", em.getFechaNacimiento())
                .append("genero", em.getGenero())
                .append("email", em.getEmail())
                .append("telefono", em.getTelefono())
                .append("contrasena", em.getContrasena())
                .append("nombreUsuario", em.getNombreUsuario())
                .append("estatus", em.getEstatus())
                .append("rol", em.getRol())
                .append("foto", em.getFoto())
                .append("fechaRegistro", em.getFechaRegistro())
                .append("claveAcceso", generadorClave());
        conexion.getMongodb().getCollection("empleado").insertOne(empleado);
    }

    public ArrayList busquedaEmpleados(String busqueda) {
        BasicDBObject query = new BasicDBObject();
        query.append("nombre", busqueda);
        ArrayList documentos = new ArrayList();
        FindIterable<Document> iterable = conexion.getMongodb().getCollection("empleado").find(query);
        iterable.forEach(new Block<Document>() {
            @Override
            public void apply(final Document document) {
                documentos.add(document);
            }
        });
        return documentos;
    }

    public void actualizarEmpleado(Empleado em) {
        BasicDBObject empleadoNuevo = new BasicDBObject();
        BasicDBObject empleadoActual = new BasicDBObject();
        
        UpdateOptions uo = new UpdateOptions();
        uo.upsert(false);
        
        empleadoNuevo.append("$set",
                new BasicDBObject()
                        .append("nombre", em.getNombre())
                        .append("apellidoPa", em.getApellidoPa())
                        .append("apellidoMa", em.getApellidoMa())
                        .append("fechaNacimiento", em.getFechaNacimiento())
                        .append("genero", em.getGenero())
                        .append("email", em.getEmail())
                        .append("telefono", em.getTelefono())
                        .append("contrasena", em.getContrasena())
                        .append("nombreUsuario", em.getNombreUsuario())
                        .append("estatus", em.getEstatus())
                        .append("rol", em.getRol())
                        .append("foto", em.getFoto()));
        
        empleadoActual.append("claveAcceso", em.getClaveAcceso());
        
        conexion.getMongodb().getCollection("empleado").updateOne(empleadoActual, empleadoNuevo ,uo);
    }
    
    public String generadorClave() throws NoSuchAlgorithmException {
        String[] symbols = {"0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "a", "b", "c", "d", "e", "f"};
        int length = 20;
        Random random = SecureRandom.getInstanceStrong(); // as of JDK 8, this should return the strongest algorithm available to the JVM
        StringBuilder sb = new StringBuilder(length);
        for (int i = 0; i < length; i++) {
            int indexRandom = random.nextInt(symbols.length);
            sb.append(symbols[indexRandom]);
        }
        String password = sb.toString();
        
        return password;
    }
    
    
    public void eliminarEmpleado(String clave){
        BasicDBObject query = new BasicDBObject();
        BasicDBObject queryBuscar = new BasicDBObject();
        
        UpdateOptions uo = new UpdateOptions();
        uo.upsert(false);
        
        query.append("$set",
                new BasicDBObject()
                        .append("estatus", "Inactivo"));
        
        queryBuscar.append("claveAcceso", clave);
        
        conexion.getMongodb().getCollection("empleado").updateOne(queryBuscar, query,uo);
    }
}
